import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, FlatList, Image } from 'react-native';
import { Audio } from 'expo-av';
import { useState } from 'react';
import * as FileSystem from 'expo-file-system'; 
import { Rings } from './components/Rings';
import { createStackNavigator } from '@react-navigation/stack';
import { Routes } from './types/Routes';
import { LoadAssets } from './components/LoadAssets';

interface responseStruct {
  id: number;
  name: string;
  imageUrl: string;
}

export default function App() {
  const [recording, setRecording] = useState<Audio.Recording>();
  const [isRecording, setIsRecording] = useState<boolean>(false);
  const [data, setData] = useState<responseStruct[]>([]);
  const [showButton, setShowButton] = useState(true);

  async function startRecording() {
    try {
      setProgress(true);
      console.log("Requesting permission")
      await Audio.requestPermissionsAsync()
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: true,
        playsInSilentModeIOS: true,
      });

      console.log("Starting recording")
      const { recording } = await Audio.Recording.createAsync(Audio.RecordingOptionsPresets.HIGH_QUALITY);
      setRecording(recording)
      console.log("Recording started")
    } catch (err) {
      console.log("Failed to start recording", err)
    }
  }

  async function stopRecording() {
    const server_url = 'http://192.168.15.190:8000/uploadfile'
    console.log("Stopping recording")
    setRecording(undefined)
    setProgress(false);
    await recording?.stopAndUnloadAsync()
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
    })
    const audioFileUri = recording?.getURI();
    if (audioFileUri) {
      const response = await FileSystem.uploadAsync(server_url, audioFileUri, {
        fieldName: 'file',
        httpMethod: 'POST',
        uploadType: FileSystem.FileSystemUploadType.MULTIPART,
      });
      console.log(response)
      const parsed = JSON.parse(response.body);
      console.log([parsed.movie])
      setData([parsed.movie])
      setIsRecording(false);
      setShowButton(false);
    }
    console.log("Stopped recording")

  }

  const fonts = {};
  const assets: number[] = [];
  const Stack = createStackNavigator<Routes>();
  const AppNavigator = ({render}:{render: boolean}) => (
    <Stack.Navigator>
      <Stack.Screen
        name="Rings"
        component={() => <Rings render={render} />}
        options={{
          title: "🏋️‍♂️ Fitness Rings",
          headerShown: false,
        }}
       />
    </Stack.Navigator>
  );

  const [progress, setProgress] = useState<boolean>(false);

  return (
      <LoadAssets assets={assets} fonts={fonts}>
        <AppNavigator render={progress} />
        {showButton ? <View style={{backgroundColor: 'black'}}>
          <View style={{marginBottom: 150, width: 150, marginLeft: 110}}>
            <Button 
            onPress={recording ? stopRecording : startRecording} 
            title={'FIND MY MOVIE'}
            /> 
             <StatusBar style="auto" />
          </View>
        </View> :
        <View style={styles.movieContainer}>
          <FlatList 
          data={data} 
          renderItem={({ item }) => 
            <View style={styles.flatListContainer}>
              <Image source={{uri: item.imageUrl}}
              style={{height:200, width:'100%'}} />
              <Text style={styles.text}>
                {item.name}
              </Text>
              <View style={{marginTop: 25}}>
                <Button 
                onPress={() => showButton ? setShowButton(false):setShowButton(true)} 
                title={'GO BACK'}
                /> 
              </View>
            </View>
          } 
          keyExtractor={(item) => item.id.toString()}/>
        </View>}
      </LoadAssets>
  );
}

const styles = StyleSheet.create({
  movieContainer: {
    height: 1000,
    backgroundColor: 'black'
  },
  flatListContainer: {
    backgroundColor: "#70a1ff",
    marginTop: 250,
    marginVertical: 10,
    marginHorizontal: 16,
    paddingBottom: 32,
    borderRadius: 6,
    justifyContent: "center",
    alignItems: "center"
  },
  separator: {
    height: 2,
    backgroundColor: "#f1f2f6"
  },
  text: {
    fontSize: 24,
    paddingTop: 6,
  },
  container: {
    flex: 1,
    backgroundColor: '#E43610',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
